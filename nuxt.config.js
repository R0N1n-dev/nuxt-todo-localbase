export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: "static",
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "nuxt-todo",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Localbase Todo" },
      { name: "format-detection", content: "telephone=no" },
      { name: "theme-color", content: "#3d5191" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~/assets/maintodo.css", "~/assets/spinner.css", "~/assets/sync.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/localbase", mode: "client" },
    { src: "~/plugins/pwa-update", mode: "client" }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    "@nuxtjs/pwa"
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios

  // PWA module configuration: https://go.nuxtjs.dev/pwa

  // Build Configuration: https://go.nuxtjs.dev/config-build
  pwa: {
    manifest: {
      name: "Rozen Nuxt pwa",
      short_name: "RoNuxtWA",
      lang: "en",
      theme_color: "#3d5191",
    },
  },
  build: {
    html: {
      minify: {
        minifyJS: true,
        minifyCSS: true,
        //preserveLineBreaks: false,
        //collapseWhiteSPace: true,
      },
    },
  },
};
